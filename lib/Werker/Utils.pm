package Werker::Utils {
	use Moose;
	use Moose::Exporter;

	use File::ShareDir;

	Moose::Exporter->setup_import_methods(
		as_is => [ qw( dist_dir ) ]
	);

	sub dist_dir { File::ShareDir::dist_dir( 'Werker' ) }

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__

=head1 NAME

Werker::Utils

=head1 DESCRIPTION

=head1 METHODS

=head2 dist_dir

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
