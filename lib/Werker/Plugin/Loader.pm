package Werker::Plugin::Loader {
	use Moose;

	extends 'Mojolicious::Plugin';

	use Mojo::Loader qw( load_class );

	sub register {
		my ( $self, $app, $config ) = @_;

		$app->attr( '_flows' => sub {
				my $result = {};

				foreach my $key ( keys( %{ $config || {} } ) ) {
					my $class = $config->{ $key }->{type};

					load_class( $class );
					unless( $class->isa( 'Werk::Flow' ) ) {
						$app->log()->warn( sprintf( 'Class "%s" is not a flow!', $class ) );
						next();
					}

					$result->{ $key } = $class->new(
						title => $key,
						%{ $config->{ $key }->{args} || {} },
					);
				}

				return $result;
			}
		 );

		 $app->helper( 'flows' => sub { shift()->app()->_flows() } );

	}

	__PACKAGE__->meta()->make_immutable( inline_constructor => 0 );
}

1;

__END__

=head1 NAME

Werker::Plugin::Loader

=head1 DESCRIPTION

=head1 METHODS

=head2 register

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
