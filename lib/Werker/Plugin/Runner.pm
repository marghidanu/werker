package Werker::Plugin::Runner {
	use Moose;

	extends 'Mojolicious::Plugin';

	use Werk::ExecutorFactory;

	sub register {
		my ( $self, $app, $config ) = @_;

		my $executor = Werk::ExecutorFactory->create(
			$config->{type} || 'Local',
			$config->{args} || { parallel_tasks => 0 },
		);

		$app->minion()->add_task( run_flow => sub {
				my ( $job, $name, $args ) = @_;

				eval {
					my $flow = $job->app()->_flows()->{ $name };
					die( sprintf( '%s flow was not found!', $name ) )
						unless( defined( $flow ) );

					my $context = $executor->execute( $flow, $args || {} );
					$job->finish( $context->results() );
				};

				$job->fail( { error => $@ } )
					if( $@ );
			}
		);
	}

	__PACKAGE__->meta()->make_immutable( inline_constructor => 0 );
}

1;

__END__

=head1 NAME

Werker::Plugin::Runner

=head1 DESCRIPTION

=head1 METHODS

=head2 register

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
