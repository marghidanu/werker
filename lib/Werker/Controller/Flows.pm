package Werker::Controller::Flows {
	use Moose;

	extends 'Mojolicious::Controller';

	use Mojo::File qw( tempfile );

	sub list {
		my $self = shift();

		my @flows = sort( keys( %{ $self->flows() } ) );

		return $self->render( json => \@flows );
	}

	sub retrieve {
		my $self = shift();

		my $flow = $self->stash( 'flow' );

		return $self->render( json => {} );
	}

	sub draw {
		my $self = shift();

		my $flow = $self->stash( 'flow' );
		my $path = tempfile();

		$flow->draw( $path->to_string() );

		return $self->render(
			data => $path->slurp(),
			format => $self->req()->query_params()->param( 'format' ) || 'svg',
		);
	}

	sub run {
		my $self = shift();

		my $flow = $self->stash( 'flow' );

		my $id = $self->minion()
			->enqueue( run_flow => [ $flow->title(), $self->req()->json() ],
				{
					notes => {
						flow_id => $flow->title()
					}
				}
			);

		return $self->render( json => {
				job_id => $id,
				flow => $flow->title(),
				created => time(),
			}
		);
	}

	sub details {
		my $self = shift();

		my $flow = $self->flows()->{ $self->param( 'name' ) };
		$self->reply()->not_found() && return undef
			unless( defined( $flow ) );

		$self->stash( flow => $flow );

		return 1;
	}

	__PACKAGE__->meta()->make_immutable( inline_constructor => 0 );
}

1;

__END__

=head1 NAME

Werker::Controller::Flows

=head1 DESCRIPTION

=head1 METHODS

=head2 list

=head2 retrieve

=head2 draw

=head2 run

=head2 details

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
