package Werker::Controller::Jobs {
	use Moose;

	extends 'Mojolicious::Controller';

	sub list {
		my $self = shift();

		my $stats = $self->minion()
			->stats();

		return $self->render( json => $stats );
	}

	sub status {
		my $self = shift();

		my $job = $self->minion()
			->job( $self->param( 'job_id' ) );

		return $self->reply()->not_found()
			unless( defined( $job ) );

		return $self->render( json => $job->info() );
	}

	__PACKAGE__->meta()->make_immutable( inline_constructor => 0 );
}

1;

__END__

=head1 NAME

Werker::Controller::Jobs

=head1 DESCRIPTION

=head1 METHODS

=head2 list

=head2 status

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
