package Werker::Controller {
	use Moose;

	extends 'Mojolicious::Controller';

	sub index {
		my $self = shift();

		return $self->render( json => {} );
	}

	__PACKAGE__->meta()->make_immutable( inline_constructor => 0 );
}

1;

__END__

=head1 NAME

Werker::Controller::Default

=head1 DESCRIPTION

=head1 METHODS

=head2 index

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
