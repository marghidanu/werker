package Werker {
	use Moose;

	extends 'Mojolicious';

	use Werker::Utils qw( dist_dir );

	use Mojo::File qw( tempdir );

	our $VERSION = '1.0';

	sub startup {
		my $self = shift();

		# Configuration ...
		push( @{ $self->plugins()->namespaces() }, 'Werker::Plugin' );

		# Plugins ...
		my $config = $self->plugin( 'JSONConfig',
			{
				file => sprintf( '%s/config/werker.%s.json', dist_dir(), $self->mode() )
			}
		);

		# TODO: Update allow settings to be configurable.
		$self->plugin( 'ServerStatus',
			{
				path => '/status',
				allow => [ '0.0.0.0/0' ],
				scoreboard => tempdir(),
			}
		);

		$self->plugin( 'Minion', $config->{minion} || {} );
		$self->plugin( 'Minion::Admin', {} );

		# Custom plugins
		$self->plugin( 'Loader', $config->{flows} || {} );
		$self->plugin( 'Runner', $config->{executor} || {} );

		$self->_routes();
	}

	sub _routes {
		my $self = shift();

		$self->routes()->get( '/' )->to( 'Default#index' );

		my $api = $self->routes()->any( '/v1' );

		# --- Flows
		$api->get( '/flows' )->to( 'Flows#list' );

		my $flow_details = $api->under( '/flows/:name' )->to( 'Flows#details' );
		$flow_details->get( '/' )->to( 'Flows#retrieve' );
		$flow_details->put( '/' )->to( 'Flows#run' );
		$flow_details->get( '/draw' )->to( 'Flows#draw' );

		# --- Jobs
		$api->get( '/jobs' )->to( 'Jobs#list' );
		$api->get( '/jobs/:job_id' )->to( 'Jobs#status' );
	}

	__PACKAGE__->meta()->make_immutable( inline_constructor => 0 );
}

1;

__END__

=head1 NAME

Werker

=head1 DESCRIPTION

=head1 METHODS

=head2 startup

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
