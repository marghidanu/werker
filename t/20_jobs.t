#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;
use Test::Mojo;

use lib 't/lib';

my $t = Test::Mojo->new( 'Werker' );

$t->get_ok( '/v1/jobs' )
	->status_is( 200 );

done_testing();
