#!/usr/bin/env perl

use strict;
use warnings;

use lib 't/lib';

use Test::More;
use Test::Mojo;

my $t = Test::Mojo->new( 'Werker' );

$t->get_ok( '/v1/flows' )
	->status_is( 200 )
	->content_type_like( qr/application\/json/ )
	->json_is( [ 'flow1', 'flow2' ] );

$t->get_ok( '/v1/flows/flow1' )
	->status_is( 200 )
	->content_type_like( qr/application\/json/ );

$t->get_ok( '/v1/flows/flow1/draw' )
	->status_is( 200 );

done_testing();
