#!1/usr/bin/env perl

use strict;
use warnings;

use Test::More;
use Test::Mojo;

use lib 't/lib';

my $t = Test::Mojo->new( 'Werker' );

my $data = {
	url => 'https://www.cpexecutive.com/post/bridge-office-sells-460-ksf-metro-atlanta-complex/'
};

$t->put_ok( '/v1/flows/flow1', json => $data )
	->status_is( 200 )
	->content_type_like( qr/application\/json/ )
	->json_has( '/job_id' )
	->json_has( '/flow' )
	->json_has( '/created' )
	->json_is( '/flow', 'flow1' );

$t->app()->minion()->perform_jobs();

done_testing();
