package WerkX::Flow1 {
	use Moose;

	extends 'Werk::Flow';

	use Mojo::UserAgent;
	use Mojo::ByteStream qw( b );

	use Werk::Task::Code;
	use Werk::Task::Dummy;

	sub BUILD {
		my $self = shift();

		my $download = Werk::Task::Code->new( id => 'download', code => \&download );
		my $content = Werk::Task::Code->new( id => 'content', code => \&content );
		my $sentences = Werk::Task::Code->new( id => 'sentences', code => \&sentences );
		my $people = Werk::Task::Code->new( id => 'people', code => \&people );
		my $times = Werk::Task::Code->new( id => 'times', code => \&times );

		my $save = Werk::Task::Dummy->new( id => 'save' );

		$self->add_deps( $download, $content );
		$self->add_deps( $content, $sentences, $people, $times );
		$self->add_deps_up( $save, $sentences, $people, $times );

		foreach my $task ( $self->get_tasks() ) {
			$task->on( before_task => sub {
					printf( "Starting task %s\n", shift->id() );
				}
			);

			$task->on( after_task => sub {
					printf( "Done with %s\n", shift()->id() );
				}
			);
		}
	}

	sub download {
		my ( $t, $c ) = @_;

		return Mojo::UserAgent->new()
			->get( $c->get_global( 'url' ) )
			->result()
			->body();
	}

	sub content {
		my ( $t, $c ) = @_;

		my $body = b( $c->get_result_path( '/download' ) )
			->encode();

		return Mojo::UserAgent->new()
			->post( 'http://www.datasciencetoolkit.org/html2story' => $body )
			->result()
			->json( '/story' );
	}

	sub sentences {
		my ( $t, $c ) = @_;

		my $body = b( $c->get_result_path( '/content' ) )
			->encode();

		return Mojo::UserAgent->new()
			->post( 'http://www.datasciencetoolkit.org/text2sentences' => $body  )
			->result()
			->json( '/sentences' );
	}

	sub people {
		my ( $t, $c ) = @_;

		my $body = b( $c->get_result_path( '/content' ) )
			->encode();

		return Mojo::UserAgent->new()
			->post( 'http://www.datasciencetoolkit.org/text2people' => $body )
			->result()
			->json();
	}

	sub times {
		my ( $t, $c ) = @_;

		my $body = b( $c->get_result_path( '/content' ) )
			->encode();

		return Mojo::UserAgent->new()
			->post( 'http://www.datasciencetoolkit.org/text2times' => $body )
			->result()
			->json();
	}

	__PACKAGE__->meta()->make_immutable();
}

1;
