# werker

## Build status

[![Build Status](https://travis-ci.org/marghidanu/werker.svg?branch=master)](https://travis-ci.org/marghidanu/werker)

## Description

REST interface for **Werk** flows and distributed execution via **Minion**.

## Example

## Installation

### Development environment

```bash
	vagrant up
	vagrant ssh

	sudo su -
	cd /vagrant

	perl Build.PL
	./Build installdeps
	./Build
	./Build test
```

### Building it from source

Make sure you install the **Werk** package first. This has to be done manually since is not uploaded (yet) to CPAN.

```bash
	cpanm --notest git://github.com/marghidanu/werk.git
```

After that is done you can proceed with installing the package.

```bash
	perl Build.PL
	./Build installdeps
	./Build manifest
	./Build
	./Build test
	./Build install
```

### Installing from Github

```bash
	cpanm --notest git://github.com/marghidanu/werk.git
	cpanm --notest git://github.com/marghidanu/werker.git
```
